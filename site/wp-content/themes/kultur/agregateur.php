<?php 
/** 
 * @package WordPress 
 * @subpackage Kultur_Theme
 */ 
 /*
Template Name: Agregateur Pages
*/ 

get_header(); ?>

<div id="content">
<div id="content_page">
<div id="breadcrumb">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
	</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div style="padding: 5px; padding-left: 20px; padding-top: 15px; margin-bottom: 10px; text-transform: lowercase; font-size: 20pt;"><? the_title() ?></div>
<? if(get_the_content() != ""){ ?><div style="padding: 5px; margin-left: 20px; padding-left: 0px;border-bottom:1px dashed black; font-size: 10pt; line-height: 18px;"><?php the_content(); ?></div><? } ?>
<? endwhile; endif; ?>
<?
global $post;
if ($post->ID == 82) 
	$myposts = get_posts("post_parent=".$post->ID."&post_type=page&numberposts=-1&order=ASC");
else
	$myposts = get_posts("post_parent=".$post->ID."&post_type=page&numberposts=-1&orderby=menu_order&order=ASC&exclude=134");
 foreach($myposts as $post) {
 setup_postdata($post);
?>
<div style="padding: 5px; margin-left: 20px; padding-left: 0px; padding-top: 15px; padding-bottom: 15px; margin-bottom: 10px; border-bottom: 1px dashed;">
	<div style="font-size: 16pt; text-transform: lowercase;"><a href="<?php the_permalink(); ?>" class="postMore"><?php the_title(); ?></a></div><span style="line-height: 18px;">
	<? the_advanced_excerpt('length=40&use_words=1'); ?>
</span>
</div>
<? } ?>
</div>
</div>
<? 
get_footer();
?>
