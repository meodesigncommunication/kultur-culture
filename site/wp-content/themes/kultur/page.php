<?php 
/** 
 * @package WordPress 
 * @subpackage Kultur_Theme
 */ 
 /*
Template Name: Main pages
*/ 

get_header(); ?>

<div id="content">
<div id="content_page">
<div id="breadcrumb">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div style="padding: 5px; padding-left: 20px; padding-top: 15px; margin-bottom: 10px; text-transform: lowercase; font-size: 20pt;"><? the_title(); ?></div>
<div style="padding: 5px; padding-left: 20px; padding-top: 15px; margin-bottom: 10px;"><? the_content() ?></div>
<? endwhile; endif; ?>
</div>
</div>
<? 
get_footer();
?>
