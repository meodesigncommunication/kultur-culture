<?php
/**
 * @package WordPress
 * @subpackage Kultur_Theme
 */

// Ensure we don't have any whitespace in the menus, as IE can be tempermental
add_filter('wp_list_pages', 'kc_trim_whitespace');
function kc_trim_whitespace($output) {
	$output = preg_replace('/\t|\n|\r/', '', $output);
	return $output;
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<?php
if ( defined('MEO_DEBUG') && MEO_DEBUG ) {
?>
   <script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script>
<?php
}
?>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" media="screen" />
	<!--[if gte IE 7]>
	<style>
	.sf-menu a {
	    zoom: 1; /* fixes IE issue where page underneath is detected on rollover.  Doesn't work on IE6 */
	}
	</style>
	<![endif]-->
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/superfish.js" type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/resizing_background.js" type="text/javascript"></script>
	<?php wp_head(); ?>
<script type="text/javascript">

    $(document).ready(function() {

    	$(".sf-menu li ul li ul").remove();

        $('ul.sf-menu').superfish({
            delay:       800,
            speed:       'slow',
            autoArrows:  false,
            dropShadows: false
        });
    });

</script>
<?php comments_popup_script(400, 500); ?>
</head>
<body onLoad="rbInit()" onResize="rbResize()">
<script type="text/javascript">
// "true" means "keep the proportions of the original image."
// If you pass "false" the image fills the whole window,
// even if it must be distorted to do so. Experiment.
rbOpen(true);
</script>
<?
global $lang;
echo $lang ?>
<div id="container">
	<div id="header">
		<div id="logo_kultur">
			<div id="logos">
				<a href="http://www.be.ch" target="_blank"><img width="160" src="<?php bloginfo('template_url'); ?>/images/logo_be.png" alt="Bern" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="http://www.vd.ch" target="_blank"><img width="90" src="<?php bloginfo('template_url'); ?>/images/logo_vd.png" alt="Vaud" /></a>
			</div>
		</div>
		<div id="menu">
			<div id="menuRoll">
				<ul class="sf-menu"><?php
					wp_list_pages('sort_column=menu_order&title_li=&sort_column=menu_order&exclude_tree=87,133');
				?></ul>
			</div>
			<div id="langSelect"><?php
				qtrans_generateLanguageSelectCode('text');
			?></div>
		</div>
	</div>
