<?php
/**
 * @package WordPress
 * @subpackage Kultur_Theme
 */
 /*
Template Name: Agregateur Beneficiaires
*/

get_header();

define('FROM_DATE_FIELD', 'from_date');
define('TO_DATE_FIELD', 'to_date');

?>

<div id="content">
<div id="content_page">
<div id="breadcrumb">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
	</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div style="padding: 5px; padding-left: 20px; padding-top: 15px; margin-bottom: 10px; text-transform: lowercase; font-size: 20pt;"><? the_title() ?></div>
<? if(get_the_content() != ""){ ?><div style="padding: 5px; margin-left: 20px; padding-left: 0px;border-bottom:1px dashed black; font-size: 10pt; line-height: 18px;"><?php the_content(); ?></div><? } ?>
<? endwhile; endif; ?>
<?php

 $querystr = "
    SELECT DISTINCT wposts.ID, wposts.*
    FROM $wpdb->posts wposts
        LEFT JOIN $wpdb->postmeta fromdate ON wposts.ID = fromdate.post_id AND fromdate.meta_key = '".FROM_DATE_FIELD."'
        LEFT JOIN $wpdb->postmeta todate   ON wposts.ID = todate.post_id   AND todate.meta_key = '".TO_DATE_FIELD."'
    WHERE wposts.post_status = 'publish'
        AND wposts.post_type = 'page'
        AND wposts.post_parent = '87'
        AND IFNULL(todate.meta_value, fromdate.meta_value) < CURDATE()
    ORDER BY IFNULL(fromdate.meta_value, todate.meta_value) ASC
 ";

$myposts = $wpdb->get_results($querystr, OBJECT);

foreach($myposts as $post) {
	?><div style="padding: 10px; padding-top: 0px; margin: 10px; border-bottom: 1px dashed;"><?
	setup_postdata($post);
	$attachments =& get_children( 'post_type=attachment&post_mime_type=image&post_parent='.$post->ID);
	$imgPrincipale =false;
	foreach($attachments as $attachment => $attachment_array) {
		$imagearray = wp_get_attachment_image_src($attachment, 'thumbnail', false);
		$imageURI = $imagearray[0];
		$imageID = get_post($attachment);
		$imageTitle = $imageID->post_title;
		$imageDescription = $imageID->post_content;
		$imageDescription2 = sanitize_title($imageDescription);
		if($imageDescription2 == 'main-picture'){
				$imgPrincipale =true;
				$uriImgPrincipale = $imageURI;
				$titleImgPrincipale = $imageTitle;
				$imageWidth = $imagearray[1];
				$imageHeight = $imagearray[2];
		}
	}
	if ($imgPrincipale) {
		?><div style="float:left; width: <?php echo $imageWidth; ?>px; height: <?php echo $imageHeight; ?>px; margin-right: 5px; background-image:url(<?php echo $uriImgPrincipale; ?>)"></div><?
	}
	$from_date = get_meta(FROM_DATE_FIELD);
	$to_date = get_meta(TO_DATE_FIELD);

	$date_string = '';
	if (!empty($from_date)) {
		$date_string = date("d.m.Y",strtotime($from_date));
		if (!empty($to_date)) {
			$date_string .= ' - ';
		}
	}
	if (!empty($to_date)) {
		$date_string .= date("d.m.Y",strtotime($to_date));
	}
	?>
	<div style="line-height: 14pt;"><?php echo $date_string; ?><br />
	<span style="text-transform: uppercase; font-size: 12pt; font-weight: bold;"><? the_title(); ?></span><br /><?php echo get_meta('where'); ?>&nbsp;|&nbsp;<?php echo get_meta('city'); ?></div>
	</div>

<?
}
 ?>

</div>
</div>
<?
get_footer();
?>
