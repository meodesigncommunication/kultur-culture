<?php
/**
 * @package WordPress
 * @subpackage Kultur_Theme
 */
 /*
Template Name: Home
*/

get_header();

define('FROM_DATE_FIELD', 'from_date');
define('TO_DATE_FIELD', 'to_date');

?>

<div id="content">
<div id="home_left">
		<?php $my_id = 1;
		$post = get_post($my_id);
		setup_postdata($post);
		if (!empty($_GET['lang'])) {
		?><a href="?page_id=69&amp;lang=<?php echo qtrans_getLanguage(); ?>">
		<?php } else{
		?><a href="?page_id=69"><?php } ?>
		<div style="background-color: rgb(255,255,255); filter:alpha(opacity=80); opacity:0.8; padding: 5px; padding-left: 20px; padding-top: 15px; height: 115px; color: black;"><span style="text-transform: lowercase; font-size: 20pt"><?php the_title(); ?></span><?php the_content(); ?></div></a>

		<?php $my_id = 158;
		$post = get_post($my_id);
		setup_postdata($post);
		if (!empty($_GET['lang'])) {
		?><a href="?page_id=157&amp;lang=<?php echo qtrans_getLanguage(); ?>">
		<?php } else{
		?><a href="?page_id=157"><?php } ?>
		<div style="background-color: rgb(255,255,255); filter:alpha(opacity=80); opacity:0.8; margin-top: 4px; padding: 5px; padding-left: 20px; padding-top: 15px; height: 81px; color: black;"><span style="text-transform: lowercase; font-size: 20pt"><?php the_title(); ?></span><?php the_content(); ?></div></a>
		<?php $my_id = 8;
		$post = get_post($my_id);
		setup_postdata($post);
		if (!empty($_GET['lang'])) {
		?><a href="?page_id=15&amp;lang=<?php echo qtrans_getLanguage(); ?>">
		<?php } else{
		?><a href="?page_id=15"><?php } ?>
		<div style="background-color: rgb(255,255,255); filter:alpha(opacity=80); opacity:0.8; margin-top: 4px; padding: 5px; padding-left: 20px; padding-top: 15px; height: 81px; color: black;"><span style="text-transform: lowercase; font-size: 20pt"><?php the_title(); ?></span><?php the_content(); ?></div></a>
</div>
<div style="clear: both;"></div>
</div>
<?php
$attachments =& get_children( 'post_type=page&post_parent=82&numberposts=3&orderby=menu_order&order=ASC');?>
<div id="communication">
<span style="text-transform: lowercase; font-size: 20pt; color: white;"><?php _e("kultur_communication") ?></span><br />
<?php foreach($attachments as $post) {
setup_postdata($post);
	 if ($post->ID != 134)  {?>

<strong><?php the_title(); ?></strong><br />
<?php the_content(); ?>

<?php
	}
} ?></div>

<?php
get_footer();
?>
